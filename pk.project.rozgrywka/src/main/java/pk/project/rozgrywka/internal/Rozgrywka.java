package pk.project.rozgrywka.internal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import pk.project.plansza.IPlansza;
import pk.project.contracts.IGra;
import pk.project.contracts.IStatek;
import pk.project.contracts.ISztuczna;
import pk.project.contracts.IFabryka;
import pk.project.contracts.IWyniki;
import pk.project.muzyka.IMuzyka;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

@Service
@Component
public class Rozgrywka implements IGra {

    private BundleContext context;

    @Reference
    private IStatek statek;
    @Reference
    private IPlansza planszaUzytkownika;
    @Reference
    private IPlansza planszaKomputera;
    @Reference
    private IFabryka fabryka;
    @Reference
    private ISztuczna sztuczna;
    @Reference
    private IWyniki wyniki;
    @Reference
    private IMuzyka muzyka;

    private int czas;
    private int liczbaJednomasztowcow;
    private int liczbaDwumasztowcow;
    private int liczbaTrzymasztowcow;
    private int liczbaCzteromasztowcow;
    private int licznik;
    private int l4;
    private int l3;
    private int l2;
    private int l1;
    private int sumaStatkow;
    private int liczbaStrzalow;
    private int liczbaStatkow;
    private int dlugoscStatku;
    private int liczbaTrafionych;
    private int punkty;
    private Ruch czyjRuch;
    private boolean czyDzwiek;
    private boolean czyUstawione;
    private boolean graczWygral;
    private boolean seria;
    private Color[] zestawColorow;

    private long start;
    private JFrame frame;
    JButton s4 = new JButton("Dodaj 4-masztowiec.");
    JButton s3 = new JButton("Dodaj 3-masztowiec.");
    JButton s2 = new JButton("Dodaj 2-masztowiec.");
    JButton s1 = new JButton("Dodaj 1-masztowiec.");
    private JLabel czasV;
    private JLabel czasT;
    private Timer timer;
    private JLabel ruch;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Rozgrywka zaladowany.");
        this.zestawColorow = new Color[3];
        this.czyUstawione = false;
        this.seria = false;
        this.czyjRuch = Ruch.Uzytkownik;
        this.liczbaStrzalow = 0;
        this.liczbaTrafionych = 0;

    }

    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Rozgrywka wylaczony.");

    }

    protected void bindStatek(IStatek statek) {
        this.statek = statek;
    }

    protected void bindPlanszaUzytkownika(IPlansza plansza) {
        this.planszaUzytkownika = plansza;
    }

    protected void bindPlanszaKomputera(IPlansza plansza) {
        this.planszaKomputera = plansza;
    }

    protected void bindFabryka(IFabryka fabryka) {
        this.fabryka = fabryka;
    }

    protected void bindSztuczna(ISztuczna sztuczna) {
        this.sztuczna = sztuczna;
    }

    protected void bindWyniki(IWyniki wyniki) {
        this.wyniki = wyniki;
    }    
        
    protected void bindMuzyka(IMuzyka muzyka)
    {
        this.muzyka = muzyka;
    }

    @Override
    public void UstawLiczbeStatkow(int liczbaJednomasztowcow, int liczbaDwumasztowcow, int liczbaTrojmasztowcow, int liczbaCzteromasztowcow) {
        this.l1 = liczbaJednomasztowcow;
        this.l2 = liczbaDwumasztowcow;
        this.l3 = liczbaTrojmasztowcow;
        this.l4 = liczbaCzteromasztowcow;
        this.sumaStatkow = liczbaJednomasztowcow + liczbaDwumasztowcow * 2 + liczbaTrojmasztowcow * 3 + liczbaCzteromasztowcow * 4;
        this.liczbaStatkow = this.sumaStatkow;
    }

    @Override
    public void StartGry() {
        PrzypiszLiczby();
        this.Ustawianie();
    }

    @Override
    public void StopGry() {
        TimerStop();
        PrzypiszLiczby();
    }

    @Override
    public boolean SprawdzCzyKoniec() {
        if (this.liczbaTrafionych == this.sumaStatkow) {
            this.graczWygral = true;
            return true;
        } else if (this.liczbaStatkow == 0) {
            this.graczWygral = false;
			ZmianaRuchu();
            return true;
        }
        return false;
    }

    @Override
    public void UstawKolory(Color[] tablicaKolorow) {
        this.zestawColorow = tablicaKolorow;
    }

    @Override
    public void UstawDzwiek(boolean sound) {
        this.czyDzwiek = sound;
    }

    private void Ustawianie() {
        this.licznik = 0;
        this.dlugoscStatku = 0;
        frame = new JFrame("Ustawianie statków.");
        frame.setLayout(null);
        frame.setPreferredSize(new Dimension(550, 350));
        frame.setResizable(false);
        s4.setLocation(340, 10);
        s3.setLocation(340, 90);
        s2.setLocation(340, 170);
        s1.setLocation(340, 250);
        s4.setSize(200, 50);
        s3.setSize(200, 50);
        s2.setSize(200, 50);
        s1.setSize(200, 50);
        s4.addActionListener(al4);
        s3.addActionListener(al3);
        s2.addActionListener(al2);
        s1.addActionListener(al1);
        frame.getContentPane().add(s4);
        frame.getContentPane().add(s3);
        frame.getContentPane().add(s2);
        frame.getContentPane().add(s1);
        ruch = new JLabel();
        ruch.setLocation(330, 10);
        ruch.setFont(new Font("Verdana", Font.BOLD, 12));
        ruch.setSize(new Dimension(150, 50));
        frame.getContentPane().add(ruch);

        this.planszaUzytkownika = (IPlansza) this.fabryka.ZwrocPlansze(10, 30, 10, 10, Color.blue, null, frame);
        planszaUzytkownika.DodajActionPol(alU);

        frame.setVisible(true);
    }

    private void UstawienieKomp() {
        PrzypiszLiczby();

        while (this.liczbaCzteromasztowcow > 0) {
            DodajNMasztowce(4);
        }
        while (this.liczbaTrzymasztowcow > 0) {
            DodajNMasztowce(3);
        }
        while (this.liczbaDwumasztowcow > 0) {
            DodajNMasztowce(2);
        }
        while (this.liczbaJednomasztowcow > 0) {
            DodajNMasztowce(1);
        }

        UsunOtoczenie(planszaKomputera);

        this.planszaKomputera.DodajActionPol(alK);
    }

    private void DodajNMasztowce(int n) {
        ArrayList<Point> tablica = new ArrayList<>();
        Random r = new Random();
        int x;
        int y;
        int pi;
        int po;
        boolean pion;
        boolean straznik;

        do {
            straznik = true;
            x = r.nextInt(10);
            y = r.nextInt(10);
            pion = r.nextBoolean();
            if (pion) {
                pi = 1;
                po = 0;
            } else {
                pi = 0;
                po = 1;
            }

            for (int i = 0; i < n; i++) {
                int a = x + i * po;
                int b = y + i * pi;
                if (this.planszaKomputera.getPole(a, b) != null && this.planszaKomputera.CzyWolne(a, b)) {
                    tablica.add(new Point(a, b));
                } else {
                    straznik = false;
                    tablica.clear();
                    break;
                }
            }

        } while (!straznik);

        if (straznik) {
            statek.setLicznik(n);
            for (int i = 0; i < tablica.size(); i++) {
                statek.DodajDoPlanszy(planszaKomputera, planszaKomputera.getPole(tablica.get(i).x, tablica.get(i).y).getButton(), n, zestawColorow, false);
            }
            switch (n) {
                case 4:
                    this.liczbaCzteromasztowcow--;
                    break;
                case 3:
                    this.liczbaTrzymasztowcow--;
                    break;
                case 2:
                    this.liczbaDwumasztowcow--;
                    break;
                case 1:
                    this.liczbaJednomasztowcow--;
                    break;
            }
        }
    }

    private void ZmianaRuchu() {
        if (this.czyjRuch == Ruch.Przeciwnik) {
            this.czyjRuch = Ruch.Uzytkownik;
            this.ruch.setText("Twoj ruch.");
        } else {
            this.czyjRuch = Ruch.Przeciwnik;
            this.ruch.setText("Ruch przeciwnika.");
        }
    }

    private int AktualnaLiczbaStatkow() {
        return this.liczbaCzteromasztowcow * 4 + this.liczbaTrzymasztowcow * 3 + this.liczbaDwumasztowcow * 2 + this.liczbaJednomasztowcow * 1;
    }

    private void UsunOtoczenie(IPlansza plansza) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (plansza.getPole(i, j).getButton().getBackground() == zestawColorow[1]) {
                    plansza.getPole(i, j).UstawKolor(Color.blue);
                    plansza.getPole(i, j).UstawCzyWolne(true);
                }
            }
        }
        frame.setPreferredSize(new Dimension(800, 350));
    }

    private void AtakujPlansze(IPlansza plansza, int x, int y) {

        if (plansza.getPole(x, y).getButton().getBackground() != zestawColorow[1] && plansza.getPole(x, y).getButton().getBackground() != zestawColorow[2]) {
            if (plansza.getPole(x, y) != null && !plansza.getPole(x, y).CzyWolne()) {
                plansza.getPole(x, y).UstawKolor(zestawColorow[2]);
                boolean b = czyjRuch == Ruch.Uzytkownik;
                statek.DodajTrafienie(x, y, plansza, b);
                if(czyDzwiek)
                    muzyka.Play("hit.wav");
                if (b) {
                    if (seria) {
                        this.punkty += 10;
                    } else {
                        this.seria = true;
                    }
                    this.liczbaTrafionych++;
                    this.punkty += 10;
                } else {
                    sztuczna.CzyTrafiony(new Point(x, y), true);
                    if (statek.getCzyZatopiony()) {
                        sztuczna.CzyZatopiony(true);
                    }
                    if (statek.getLicznik() == 0) {
                        this.liczbaStatkow--;
                    }
                }
                if (SprawdzCzyKoniec()) {
                    StopGry();
                    String text;
                    if (this.graczWygral) {
                        text = "ZWYCIESTWO!";
                    } else {
                        text = "PORAZKA!";
                        this.punkty = 0;
                        PokazStatkiKomputera();
                    }
                    this.wyniki.ObliczCelnosc(this.liczbaStrzalow, this.liczbaTrafionych);

                    JOptionPane.showMessageDialog(null, text + "\nPunkty: " + this.wyniki.LiczPunkty(this.punkty, (double) this.czas, AktualnaLiczbaStatkow()));
                    frame.dispose();
                    wyniki.Podsumuj();
                }
            } else {
                if(czyDzwiek)
                    muzyka.Play("drop.wav");
                if (plansza.getPole(x, y) != null) {
                    plansza.getPole(x, y).UstawKolor(zestawColorow[1]);
                }
                if (this.czyjRuch == Ruch.Przeciwnik) {
                    sztuczna.CzyTrafiony(new Point(x, y), false);
                } else {
                    seria = false;
                }
                ZmianaRuchu();
            }
        }
    }

    private void TimerStart() {
        start = System.currentTimeMillis();
        timer = new Timer(1000, timerListener);
        timer.setInitialDelay(0);
        timer.start();
    }

    private void TimerStop() {
        timer.removeActionListener(timerListener);
    }

    public void PokazStatkiKomputera() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (planszaKomputera.getPole(i, j).getButton().getBackground() != this.zestawColorow[1] && planszaKomputera.getPole(i, j).getButton().getBackground() != this.zestawColorow[2] && !planszaKomputera.CzyWolne(i, j)) {
                    planszaKomputera.getPole(i, j).getButton().setBackground(this.zestawColorow[0]);
                }
            }
        }
    }

    private void PrzypiszLiczby() {
        this.liczbaCzteromasztowcow = l4;
        this.liczbaTrzymasztowcow = l3;
        this.liczbaDwumasztowcow = l2;
        this.liczbaJednomasztowcow = l1;
    }

    ActionListener al4 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (licznik == 0 && liczbaCzteromasztowcow > 0) {
                licznik = 4;
                statek.setLicznik(licznik);
                dlugoscStatku = 4;
            }
        }
    };
    ActionListener al3 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (licznik == 0 && liczbaTrzymasztowcow > 0) {
                licznik = 3;
                statek.setLicznik(licznik);
                dlugoscStatku = 3;
            }
        }
    };
    ActionListener al2 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (licznik == 0 && liczbaDwumasztowcow > 0) {
                licznik = 2;
                statek.setLicznik(licznik);
                dlugoscStatku = 2;
            }
        }
    };
    ActionListener al1 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (licznik == 0 && liczbaJednomasztowcow > 0) {
                licznik = 1;
                statek.setLicznik(licznik);
                dlugoscStatku = 1;
            }
        }
    };

    ActionListener alU = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!czyUstawione) {
                JButton button = (JButton) e.getSource();

                statek.DodajDoPlanszy(planszaUzytkownika, button, dlugoscStatku, zestawColorow, true);

                if (statek.getCzyOK()) {
                    switch (dlugoscStatku) {
                        case 4:
                            liczbaCzteromasztowcow--;
                            if (liczbaCzteromasztowcow == 0) {
                                frame.getContentPane().remove(s4);
                            }
                            break;
                        case 3:
                            liczbaTrzymasztowcow--;
                            if (liczbaTrzymasztowcow == 0) {
                                frame.getContentPane().remove(s3);
                            }
                            break;
                        case 2:
                            liczbaDwumasztowcow--;
                            if (liczbaDwumasztowcow == 0) {
                                frame.getContentPane().remove(s2);
                            }
                            break;
                        case 1:
                            liczbaJednomasztowcow--;
                            if (liczbaJednomasztowcow == 0) {
                                frame.getContentPane().remove(s1);
                            }
                            break;
                        default:
                            break;
                    }
                    frame.invalidate();
                    frame.validate();
                    frame.repaint();

                    if (AktualnaLiczbaStatkow() == 0) {
                        UsunOtoczenie(planszaUzytkownika);
                        czyUstawione = true;
                        planszaKomputera = (IPlansza) fabryka.ZwrocPlansze(10, 30, 480, 10, Color.blue, null, frame);
                        UstawienieKomp();

                        czasT = new JLabel("Czas: ");
                        czasT.setLocation(330, 50);
                        czasT.setSize(40, 20);
                        czasT.setVisible(true);
                        frame.getContentPane().add(czasT);
                        czasV = new JLabel("");

                        PrzypiszLiczby();
                        frame.setTitle("Zniszcz statki wroga!");
                        TimerStart();
                        ruch.setText("Twoj ruch.");
                    }
                }
                licznik = statek.getLicznik();
            }

        }

    };

    ActionListener alK = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (czyjRuch == Ruch.Uzytkownik) {
                JButton button = (JButton) e.getSource();
                if (button.getBackground() == Color.blue) {
                    liczbaStrzalow++;
                }
                int x = Character.getNumericValue(button.getName().charAt(0));
                int y = Character.getNumericValue(button.getName().charAt(1));

                AtakujPlansze(planszaKomputera, x, y);

                new Thread() {
                    public void run() {

                        while (czyjRuch == Ruch.Przeciwnik) {
                            Point atak;
                            do {
                                atak = sztuczna.Atakuj();
                                if (planszaUzytkownika.getPole(atak.x, atak.y) == null || planszaUzytkownika.getPole(atak.x, atak.y).getButton().getBackground() == zestawColorow[1]) {
                                    sztuczna.CzyTrafiony(atak, false);
                                } else {
                                    break;
                                }
                            } while (true);
                            try {
                                sleep(1500);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Rozgrywka.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            AtakujPlansze(planszaUzytkownika, atak.x, atak.y);
                        }
                    }
                }.start();
        }

    }
};

ActionListener timerListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            czasV.setLocation(375, 50);
            czasV.setSize(40, 20);
            czasV.setVisible(true);
            frame.getContentPane().add(czasV);
            long stop = System.currentTimeMillis();
            String s = String.valueOf((stop - start) / 1000);
            czas = Integer.valueOf(s);
            czasV.setText(s);
        }
    };
}
