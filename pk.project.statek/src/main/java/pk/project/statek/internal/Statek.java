package pk.project.statek.internal;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import pk.project.contracts.IStatek;
import pk.project.plansza.IPlansza;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;

@Service
@Component
public class Statek implements IStatek {

    private BundleContext context;

    private int rozmiar;
    private int licznik;
    private boolean czyOK;
    private boolean czyZatopiony;
    private boolean czyWidoczne;
    private boolean pion;
    private List<JButton> doZatwierdzenia;
    private Color[] kolory;
    private List<Point> tablicaTrafien;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Statek zaladowany.");
        this.doZatwierdzenia = new ArrayList<>();
    }
    
    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Statek wylaczony.");
    }

    @Override
    public void DodajTrafienie(int x, int y, Object plansza, boolean czyUser) {
        tablicaTrafien = new ArrayList<>();
        this.czyZatopiony = false;
        SprawdzSasiadow(x, y, plansza);

        if (SprawdzCzyWszyscy(plansza, czyUser)) {
            this.rozmiar = doZatwierdzenia.size();
            this.Otocz((IPlansza) plansza);
            doZatwierdzenia.clear();
            licznik = 0;
        }
    }

    @Override
    public void DodajDoPlanszy(Object plansza, JButton button, int dlugoscStatku, Color[] tablica, boolean czyWidoczne) {
        this.rozmiar = dlugoscStatku;
        this.kolory = tablica;
        this.czyWidoczne = czyWidoczne;
        int x = getX(button);
        int y = getY(button);
        IPlansza p = (IPlansza) plansza;
        czyOK = false;

        if (licznik > 0) {
            if (p.getPole(x, y).CzyWolne()) {
                p.getPole(x, y).UstawCzyWolne(false);
                if (this.czyWidoczne) {
                    button.setBackground(kolory[0]);
                }
                licznik--;
                doZatwierdzenia.add(button);

                if (licznik == 0) {
                    Zatwierdz(p);
                    doZatwierdzenia.clear();
                }
            }

        }
    }

    @Override
    public boolean getCzyOK() {
        return czyOK;
    }

    @Override
    public boolean getCzyZatopiony() {
        return this.czyZatopiony;
    }

    @Override
    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    @Override
    public int getLicznik() {
        return this.licznik;
    }

    private void SprawdzSasiadow(int x, int y, Object p) {
        IPlansza plansza = (IPlansza) p;
        this.pion = true;
        Point point = new Point(x, y);

        for (int i = x - 1; i >= 0 && i > x - 4; i--) {
            if (plansza.getPole(i, y) != null && !plansza.CzyWolne(i, y) && NieWybrane(i, y, plansza)) {
                tablicaTrafien.add(new Point(i, y));
                pion = false;
            } else {
                break;
            }
        }

        tablicaTrafien.add(point);

        for (int i = x + 1; i < 10 && i < x + 4; i++) {
            if (plansza.getPole(i, y) != null && !plansza.CzyWolne(i, y) && NieWybrane(i, y, plansza)) {
                tablicaTrafien.add(new Point(i, y));
                pion = false;
            } else {
                break;
            }
        }

        if (pion) {
            for (int i = y - 1; i >= 0 && i > y - 4; i--) {
                if (plansza.getPole(x, i) != null && !plansza.CzyWolne(x, i) && NieWybrane(x, i, plansza)) {
                    tablicaTrafien.add(new Point(x, i));
                } else {
                    break;
                }
            }
            if (!tablicaTrafien.contains(point)) {
                tablicaTrafien.add(point);
            }
            for (int i = y + 1; i < 10 && i < y + 4; i++) {
                if (plansza.getPole(x, i) != null && !plansza.CzyWolne(x, i) && NieWybrane(x, i, plansza)) {
                    tablicaTrafien.add(new Point(x, i));
                } else {
                    break;
                }
            }

        }
    }

    private boolean SprawdzCzyWszyscy(Object p, boolean b) {
        IPlansza plansza = (IPlansza) p;
        Color tmp;
        if (b) {
            tmp = Color.blue;
        } else {
            tmp = this.kolory[0];
        }

        for (int i = 0; i < tablicaTrafien.size(); i++) {
            doZatwierdzenia.add(plansza.getPole(tablicaTrafien.get(i).x, tablicaTrafien.get(i).y).getButton());
            if (plansza.getPole(tablicaTrafien.get(i).x, tablicaTrafien.get(i).y).getButton().getBackground() == tmp) {
                doZatwierdzenia.clear();
                return false;
            }
        }
        return true;
    }

    private int getX(JButton b) {
        return Character.getNumericValue(b.getName().charAt(0));
    }

    private int getY(JButton b) {
        return Character.getNumericValue(b.getName().charAt(1));
    }

    private void Zatwierdz(IPlansza plansza) {
        int poprzedniX;
        int aktualnyX;
        int poprzedniY;
        int aktualnyY;
        int roznicaX;
        int roznicaY;

        czyOK = true;
        pion = false;

        for (int i = 1; i < doZatwierdzenia.size(); i++) {
            aktualnyX = getX(doZatwierdzenia.get(i));
            aktualnyY = getY(doZatwierdzenia.get(i));
            poprzedniX = getX(doZatwierdzenia.get(i - 1));
            poprzedniY = getY(doZatwierdzenia.get(i - 1));

            if (i == 1) {
                pion = (aktualnyX == poprzedniX);
            }

            roznicaX = Math.abs(poprzedniX - aktualnyX);
            roznicaY = Math.abs(poprzedniY - aktualnyY);

            if ((pion && (roznicaX > 0 || roznicaY > 1)) || (!pion && (roznicaY > 0 || roznicaX > 1))) {
                czyOK = false;
                break;
            }
        }

        Color tmp;

        if (czyOK) {
            tmp = kolory[0];
            Otocz(plansza);
            licznik = 0;
        } else {
            tmp = Color.blue;
        }

        for (int i = 0; i < doZatwierdzenia.size(); i++) {
            if (this.czyWidoczne) {
                doZatwierdzenia.get(i).setBackground(tmp);
            }
            int x = getX(doZatwierdzenia.get(i));
            int y = getY(doZatwierdzenia.get(i));
            plansza.getPole(x, y).UstawCzyWolne(!czyOK);
        }
    }

    private void Otocz(IPlansza plansza) {
        JButton pierwszy = new JButton();
        JButton ostatni = new JButton();

        int startX;
        int stopX;
        int startY;
        int stopY;

        int min = 10;
        int max = -1;

        if (pion) {
            for (int i = 0; i < doZatwierdzenia.size(); i++) {
                if (getY(doZatwierdzenia.get(i)) < min) {
                    min = getY(doZatwierdzenia.get(i));
                    pierwszy = doZatwierdzenia.get(i);
                }
                if (getY(doZatwierdzenia.get(i)) > max) {
                    max = getY(doZatwierdzenia.get(i));
                    ostatni = doZatwierdzenia.get(i);
                }
            }
        } else {
            for (int i = 0; i < doZatwierdzenia.size(); i++) {
                if (getX(doZatwierdzenia.get(i)) < min) {
                    min = getX(doZatwierdzenia.get(i));
                    pierwszy = doZatwierdzenia.get(i);
                }
                if (getX(doZatwierdzenia.get(i)) > max) {
                    max = getX(doZatwierdzenia.get(i));
                    ostatni = doZatwierdzenia.get(i);
                }
            }
        }

        if (pion) {
            startX = getX(pierwszy) - 1;
            stopX = getX(pierwszy) + 1;
            startY = getY(pierwszy) - 1;
            stopY = getY(ostatni) + 1;
        } else {
            startX = getX(pierwszy) - 1;
            stopX = getX(ostatni) + 1;
            startY = getY(pierwszy) - 1;
            stopY = getY(pierwszy) + 1;
        }

        for (int i = startX; i <= stopX; i++) {
            for (int j = startY; j <= stopY; j++) {
                if (plansza.getPole(i, j) != null && plansza.getPole(i, j).CzyWolne()) {
                    plansza.getPole(i, j).UstawCzyWolne(false);
                    plansza.getPole(i, j).UstawKolor(kolory[1]);
                }
            }
        }

        this.czyZatopiony = true;
    }

    private boolean NieWybrane(int x, int y, IPlansza plansza) {
        return plansza.getPole(x, y).getButton().getBackground() != this.kolory[1];
    }
}
