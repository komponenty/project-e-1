package pk.project.sztucznainteligencja.internal;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import pk.project.contracts.ISztuczna;
import pk.project.contracts.ITrop;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

@Service
@Component
public class SztucznaInteligencja implements ISztuczna {

    private BundleContext context;

    @Reference
    private ITrop trop;

    private List<String> polaDoAtaku;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent SztucznaInteligencja zaladowany.");
        GenerujWolnePola();
    }
    
    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent SztucznaInteligencja wylaczony.");
    }

    protected void bindTrop(ITrop trop) {
        this.trop = trop;
    }

    @Override
    public Point Atakuj() {
        if (!this.trop.CzyJest()) {
            Random r = new Random();
            int x;
            int y;
            String s;

            do {
                s = this.polaDoAtaku.get(r.nextInt(polaDoAtaku.size()));
                x = Character.getNumericValue(s.charAt(0));
                y = Character.getNumericValue(s.charAt(1));
            } while (!this.polaDoAtaku.contains(x + "" + y));

            this.polaDoAtaku.remove(x + "" + y);
            return new Point(x, y);
        } else {
            return this.trop.Cel();
        }
    }

    @Override
    public void CzyTrafiony(Point point, boolean b) {

        if (b) 
            trop.ZacznijTrop(point.x, point.y);
        else if (trop.CzyJest()) 
                trop.AktualizujTrop(true);            
    }
    

    @Override
    public void CzyZatopiony(boolean b) {
        if (b) {
            trop.PrzerwijTrop();
        }
    }

    private void GenerujWolnePola() {
        this.polaDoAtaku = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.polaDoAtaku.add(i + "" + j);
            }
        }
    }
}
