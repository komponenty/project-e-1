package pk.project.plansza.internal;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.*;

import pk.project.contracts.IFabryka;
import pk.project.plansza.IPlansza;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;

@Service
@Component
public class Fabryka implements IFabryka {

    private BundleContext context;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Fabryka zaladowany.");
    }
    
    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Fabryka wylaczony.");
    }

    @Override
    public Object ZwrocPlansze(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JFrame frame) {
        IPlansza tmp = new Plansza();
        tmp.GenerujPlansze(rozmiarPlanszy, rozmiarPola, pozycjaX, pozycjaY, kolorPola, obrazek, frame);
        return tmp;
    }

}
