package pk.project.plansza.internal;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import pk.project.plansza.IPlansza;
import pk.project.plansza.IPole;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;

@Service
@Component
public class Plansza implements IPlansza {

    private BundleContext context;

    private IPole[][] tablicaPol;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Plansza zaladowany.");
    }

    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Plansza wylaczony.");
    }

    @Override
    public boolean CzyWolne(int x, int y) {
        try {
            return this.tablicaPol[x][y].CzyWolne();
        } catch (Exception e) {
            Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    @Override
    public void GenerujPlansze(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JFrame frame) {
        this.tablicaPol = new IPole[rozmiarPlanszy][rozmiarPlanszy];

        for (int i = 0; i < rozmiarPlanszy; i++) {
            for (int j = 0; j < rozmiarPlanszy; j++) {
                this.tablicaPol[j][i] = new Pole();
                this.tablicaPol[j][i].Ustaw(true, pozycjaX + (j * rozmiarPola), pozycjaY + (i * rozmiarPola), rozmiarPola, kolorPola, obrazek);
                this.tablicaPol[j][i].getButton().setName(j + "" + i);
                frame.getContentPane().add(this.tablicaPol[j][i].getButton());
            }
        }
        frame.pack();

    }
    
    @Override
    public void GenerujPlansze2(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JPanel panel) {
        this.tablicaPol = new IPole[rozmiarPlanszy][rozmiarPlanszy];

        for (int i = 0; i < rozmiarPlanszy; i++) {
            for (int j = 0; j < rozmiarPlanszy; j++) {
                this.tablicaPol[j][i] = new Pole();
                this.tablicaPol[j][i].Ustaw(true, pozycjaX + (j * rozmiarPola), pozycjaY + (i * rozmiarPola), rozmiarPola, kolorPola, obrazek);
                this.tablicaPol[j][i].getButton().setName(j + "" + i);
                panel.add(this.tablicaPol[j][i].getButton());
            }
        }
    }

    @Override
    public IPole getPole(int x, int y) {
        try {
            return this.tablicaPol[x][y];
        } catch (Exception e) {
            //Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

    @Override
    public void DodajActionPol(ActionListener listener) {
        for (int i = 0; i < this.tablicaPol.length; i++) {
            for (int j = 0; j < this.tablicaPol.length; j++) {
                this.tablicaPol[i][j].getButton().addActionListener(listener);
            }
        }
    }
}
