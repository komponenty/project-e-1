package pk.project.plansza.internal;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.*;
import pk.project.plansza.IPole;

public class Pole implements IPole {

    private int x;
    private int y;
    private int rozmiar;
    private boolean czyStatek;

    private JButton button;

    @Override
    public boolean CzyWolne() {
        return !this.czyStatek;
    }
    
    @Override
    public void UstawCzyWolne(boolean czyWolne) {
        this.czyStatek = !czyWolne;
    }

    @Override
    public void UstawKolor(Color kolor) {
        this.button.setBackground(kolor);
    }

    @Override
    public void UstawObrazek(BufferedImage obrazek) {
        Image tmp = obrazek.getScaledInstance(this.rozmiar + 6, this.rozmiar - 4, Image.SCALE_FAST);
        this.button.setIcon(new ImageIcon(tmp));
    }

    @Override
    public void Ustaw(boolean czyWolny, int pozycjaX, int pozycjaY, int rozmiarPola, Color kolorPola, BufferedImage obrazek) {
        this.czyStatek = !czyWolny;
        this.x = pozycjaX;
        this.y = pozycjaY;
        this.rozmiar = rozmiarPola;

        button = new JButton();
        button.setLayout(null);
        button.setLocation(pozycjaX, pozycjaY);
        button.setSize(rozmiarPola, rozmiarPola);
        button.setBackground(kolorPola);
        button.setVisible(true);
        if (obrazek != null) {
            this.UstawObrazek(obrazek);
        }
    }
    
    @Override
    public JButton getButton() {
        return this.button;
    }

}
