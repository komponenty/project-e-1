package pk.project.plansza;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public interface IPlansza {
    boolean CzyWolne(int x, int y);
    void GenerujPlansze(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JFrame frame);
    void GenerujPlansze2(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JPanel panel);
    IPole getPole(int x, int y);
    void DodajActionPol(ActionListener listener);
}
