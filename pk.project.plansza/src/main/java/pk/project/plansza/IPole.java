package pk.project.plansza;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.JButton;

public interface IPole {
    boolean CzyWolne();
    void UstawCzyWolne(boolean czyWolne);
    void UstawKolor(Color kolor);
    void UstawObrazek(BufferedImage obrazek);
    void Ustaw(boolean czyWolny, int pozycjaX, int pozycjaY, int rozmiarPola, Color kolorPola, BufferedImage obrazek);    
    JButton getButton();
}
