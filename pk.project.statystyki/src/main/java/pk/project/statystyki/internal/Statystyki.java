package pk.project.statystyki.internal;

import java.awt.HeadlessException;
import javax.swing.JOptionPane;
import pk.project.contracts.IWyniki;
import pk.project.database.IDatabase;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

@Service
@Component
public class Statystyki implements IWyniki {

    private BundleContext context;

    @Reference
    private IDatabase baza;

    private double punktacja;
    private double celnosc;

    private Koniec oknoKoncowe;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Statystyki zaladowany.");
    }

    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Statystyki wylaczony.");
    }

    protected void bindBaza(IDatabase baza) {
        this.baza = baza;
    }

    @Override
    public void ObliczCelnosc(double liczbaStrzalow, double liczbaTrafionych) {
        this.celnosc = liczbaTrafionych / liczbaStrzalow * 10;
    }

    @Override
    public double LiczPunkty(int punkty, double czas, int liczbaStatkow) {
        this.punktacja = Math.round((((this.celnosc * (double) punkty) / (double) liczbaStatkow) / czas) * 1000);
        return punktacja;
    }

    @Override
    public void Podsumuj() {
        this.oknoKoncowe = new Koniec(this);
        if (this.punktacja > 0) {
            this.oknoKoncowe.PrzeslijWynik(true, (int) punktacja);
        } else {
            this.oknoKoncowe.PrzeslijWynik(false, (int) punktacja);
        }
    }

    public void WyslijDane(String nick, String kraj, boolean czyM) {

        baza.Connect("db4free.net", "3306", "statki", "thr0wer", "statkiadrian");

        String plec;
        if (czyM) {
            plec = "mezczyzna";
        } else {
            plec = "kobieta";
        }

        baza.AddRecord("wyniki", nick, plec, kraj, (int) this.punktacja);
        
        JOptionPane.showMessageDialog(null, "Jezeli nie masz zablokowanych portow to Twoj wynik zostal dodany do bazy!\nWejdz na http://stateczki.cba.pl/prezentacja.php aby zobaczyc, czy jestes najlepszy!");
        this.oknoKoncowe.dispose();
    }

}
