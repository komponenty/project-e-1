package pk.project.statystyki.internal;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.*;

/**
 *
 * @author Adrian
 */
public class Koniec extends JFrame {

    private JButton wyjscie;
    
    private JLabel winOrnot;
    private JLabel wynikB;
    private JTextArea imieB;
    private JTextArea krajB;
    private JRadioButton plecB;
    private JRadioButton plecC;
    private Statystyki parent;

    public Koniec(Statystyki parent) {
        this.parent = parent;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Statki-koniec");
        setSize(400, 350);
        setVisible(true);
        setResizable(false);
        
        winOrnot = new JLabel();
        wynikB = new JLabel();
        add(winOrnot);
        winOrnot.setBounds(85, 0, 250, 50);
        winOrnot.setFont(new Font("Helvetica", Font.BOLD, 20));
        setLayout(null);

        JLabel wynikA;
        wynikA = new JLabel("Twój wynik: ");
        wynikA.setSize(100, 25);
        wynikA.setBounds(20, 70, 100, 25);
        
        wynikB.setSize(100, 25);
        wynikB.setBounds(120, 70, 100, 25);
        JLabel imieA = new JLabel("Nick:");
        imieA.setBounds(20, 95, 100, 25);
        imieA.setSize(100, 25);
        imieB = new JTextArea("nick");
        imieB.setSize(100, 25);
        imieB.setBounds(120, 95, 100, 25);
        JLabel plecA = new JLabel("Płeć:");
        plecA.setSize(100, 25);
        plecA.setBounds(20, 120, 100, 25);
        plecB = new JRadioButton("Kobieta");
        plecB.setSize(100, 25);
        plecB.setBounds(120, 120, 100, 25);
        plecC = new JRadioButton("Mężczyzna");
        plecC.setSize(100, 25);
        plecC.setBounds(220, 120, 100, 25);
        ButtonGroup group = new ButtonGroup();
        group.add(plecB);
        group.add(plecC);
        JLabel krajA = new JLabel("Kraj:");
        krajA.setSize(100, 25);
        krajA.setBounds(20, 145, 100, 25);
        krajB = new JTextArea("podaj kraj");
        krajB.setSize(100, 25);
        krajB.setBounds(120, 145, 100, 25);
        add(wynikA);
        add(wynikB);
        add(imieA);
        add(imieB);
        add(plecA);
        add(plecB);
        add(plecC);
        add(krajA);
        add(krajB);
        JButton wyslij = new JButton("Wyślij wynik");
        wyjscie = new JButton("Wyjście");

        wyslij.setSize(150, 50);
        wyslij.setBounds(40, 190, 150, 50);
        add(wyslij);
        wyjscie.setSize(150, 50);
        wyjscie.setBounds(205, 190, 150, 50);
        add(wyjscie);
        wyslij.addActionListener(al);
        wyjscie.addActionListener(al);
    }
    
    private ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == wyjscie) {
                dispose();
            }
            else{
                if("".equals(imieB.getText()) || "nick".equals(imieB.getText()) || "".equals(krajB.getText()) || "podaj kraj".equals(krajB.getText()) || (!plecB.isSelected() && !plecC.isSelected()))
                {
                      JOptionPane.showMessageDialog(null, "Uzupełnij wszystkie pola!");
                }
                else{
                    parent.WyslijDane(imieB.getText(), krajB.getText(), plecC.isSelected());
                }
            }
        }
    };
    
    public void PrzeslijWynik(Boolean rezultat, int punkty)
    {
        if (rezultat) {
            winOrnot.setText("Zwycięstwo!!!");
            wynikB.setText(""+punkty);
        }
        else{
            winOrnot.setText("Porażka!!!");
            wynikB.setText("0");
        }       
    }
}
