package pk.project.trop.internal;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import pk.project.contracts.ITrop;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

@Service
@Component
public class Trop implements ITrop {

    private BundleContext context;

    private Point pierwszeTrafienie;
    private Point aktualnaPozycja;
    private Point nastepnaPozycja;
    List<Point> tablicaSasiadow;
    private Orientacja orientacja;
    private boolean czyJest;
    private boolean lewoGora;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent Trop zaladowany.");
        this.czyJest = false;
        tablicaSasiadow = new ArrayList<>();
        nastepnaPozycja = null;
        this.czyJest = false;
    }
    
    @Deactivate
    public void deactivate() {
        this.context = null;
        System.out.println("Komponent Trop wylaczony.");
    }
    
    @Override
    public boolean CzyJest() {
        return this.czyJest;
    }
    
    @Override
    public void AktualizujTrop(boolean zmiana) {
        int i;
        if (orientacja == null) {
            Random r = new Random();

            nastepnaPozycja = tablicaSasiadow.get(r.nextInt(tablicaSasiadow.size()));
            tablicaSasiadow.remove(nastepnaPozycja);
        } else {
            this.lewoGora = nastepnaPozycja.x < pierwszeTrafienie.x || nastepnaPozycja.y < pierwszeTrafienie.y;

            if (lewoGora) {
                i = -1;
            } else {
                i = 1;
            }
            aktualnaPozycja = nastepnaPozycja;

            if (zmiana) {
                this.aktualnaPozycja = this.pierwszeTrafienie;

                i = i * (-1);
            }
            if (orientacja == Orientacja.Pozioma) {
                nastepnaPozycja = new Point(aktualnaPozycja.x + i, aktualnaPozycja.y);

            } else {
                nastepnaPozycja = new Point(aktualnaPozycja.x, aktualnaPozycja.y + i);
            }

        }

    }

    @Override
    public void ZacznijTrop(int x, int y) {
        if (!czyJest) {
            orientacja = null;
            this.czyJest = true;
            if (nastepnaPozycja == null) {
                this.pierwszeTrafienie = new Point(x, y);

                if (y > 0) {
                    tablicaSasiadow.add(new Point(pierwszeTrafienie.x, pierwszeTrafienie.y - 1));
                }
                if (y < 9) {
                    tablicaSasiadow.add(new Point(pierwszeTrafienie.x, pierwszeTrafienie.y + 1));
                }
                if (x > 0) {
                    tablicaSasiadow.add(new Point(pierwszeTrafienie.x - 1, pierwszeTrafienie.y));
                }
                if (x < 9) {
                    tablicaSasiadow.add(new Point(pierwszeTrafienie.x + 1, pierwszeTrafienie.y));
                }
            }

            AktualizujTrop(false);
        } else {
            if (this.orientacja == null) {
                UstawOrientacje();
            }
            AktualizujTrop(false);
        }
    }

    @Override
    public void PrzerwijTrop() {
        this.czyJest = false;
        this.tablicaSasiadow.clear();
        this.nastepnaPozycja = null;
        this.aktualnaPozycja = null;
        this.pierwszeTrafienie = null;
    }

    @Override
    public Point Cel() {
        return this.nastepnaPozycja;
    }

    private void UstawOrientacje() {
        if (pierwszeTrafienie.x != nastepnaPozycja.x) {
            this.orientacja = Orientacja.Pozioma;
        } else {
            this.orientacja = Orientacja.Pionowa;
        }
    }
}
