package pk.project.uzytkownik.internal;

import java.awt.Color;
import pk.project.contracts.IGra;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;

@Component(immediate = true)
public class Uzytkownik {
    
    private BundleContext context;
    
    @Reference
    private IGra rozgrywka;
    
    private Okno1 oknoStartowe;
    
   @Activate
   public void activate(BundleContext context)
   {
       this.context = context;
       System.out.println("Komponent Uzytkownik zaladowany.");   
       oknoStartowe = new Okno1(this);       
   }
   
   @Deactivate
   public void deactivate()
   {
       this.context = null;
       System.out.println("Komponent Uzytkownik wylaczony.");  
       
   }
    
    protected void bindRozgrywka(IGra rozgrywka)
    {
        this.rozgrywka = rozgrywka;
    }
    
    public void WyslijLiczby(int[] liczby)
    {
        rozgrywka.UstawLiczbeStatkow(liczby[3], liczby[2], liczby[1], liczby[0]);
        rozgrywka.StartGry();
    }
    
    public void Ustawienia(Color[] tablica, boolean sound)
    {
        rozgrywka.UstawKolory(tablica);
        rozgrywka.UstawDzwiek(sound);
    }
}
