package pk.project.uzytkownik.internal;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class Okienka extends JFrame {

    JComboBox cmb;
    JPanel k1 = new JPanel();
    JPanel k2 = new JPanel();
    JPanel k3 = new JPanel();
    Color kolor1;
    Color kolor2;
    Color kolor3;
    Boolean sound;
    JFrame oknoOpcji;
    JFrame oknoAbout;
    JFrame oknoPomocy;
    Okno1 parent;
    int ilosc4;
    int ilosc3;
    int ilosc2;
    int ilosc1;
    SpinnerModel model4;
    SpinnerModel model3;
    SpinnerModel model2;
    SpinnerModel model1;
    JFrame oknoWyboru;
    JSpinner spinner4;
    JSpinner spinner3;
    JSpinner spinner2;
    JSpinner spinner1;
    JButton runGame;
    JButton ok = new JButton("OK!");
    JCheckBox dzwiek = new JCheckBox("Dźwięk?!");

    public void setParent(Okno1 parent) {
        this.parent = parent;
    }
    ActionListener kl = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int itemIndex = cmb.getSelectedIndex();
            if (itemIndex == 0) {
                UstawDomyslne();
            } else if (itemIndex == 1) {
                k1.setBackground(Color.white);
                k2.setBackground(Color.black);
                k3.setBackground(Color.pink);
            } else if (itemIndex == 2) {
                k1.setBackground(Color.green);
                k2.setBackground(Color.darkGray);
                k3.setBackground(Color.orange);
            }
            if (e.getSource() == ok) {
                oknoOpcji.dispose();
                parent.setEnabled(true);
            }
        }

        private void StanrdoweKolory() {
            k1.setBackground(Color.yellow);
            k2.setBackground(Color.gray);
            k3.setBackground(Color.red);
        }
    };

    ActionListener rg = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            oknoWyboru.dispose();
            PobierzUstawienia();
            PrzeslijWybor();
        }
    };

    public void showOpcje() {
        oknoOpcji = new JFrame();
        oknoOpcji.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        e.getWindow().dispose();
                        parent.setEnabled(true);
                    }
                }
        );
        oknoOpcji.setSize(420, 300);
        oknoOpcji.setVisible(true);
        oknoOpcji.setTitle("Ustawienia gry");
        oknoOpcji.setResizable(false);
        oknoOpcji.setLayout(null);
        JLabel label = new JLabel("Ustawienia");
        oknoOpcji.add(label);
        label.setSize(150, 50);
        label.setBounds(135, 0, 150, 50);
        label.setFont(new Font("Helvetica", Font.BOLD, 20));
        JLabel zestaw = new JLabel("Zestaw kolorów:");
        oknoOpcji.add(zestaw);
        zestaw.setBounds(20, 60, 150, 25);
        //lista wyboru
        cmb = new JComboBox();
        cmb.setBounds(80, 40, 170, 20);
        cmb.addItem("zestaw 1");
        cmb.addItem("zestaw 2");
        cmb.addItem("zestaw 3");
        oknoOpcji.add(cmb);
        cmb.setBounds(175, 60, 150, 25);
        cmb.addActionListener(kl);
        oknoOpcji.add(k1);
        oknoOpcji.add(k2);
        oknoOpcji.add(k3);
        k1.setSize(60, 50);
        k2.setSize(60, 50);
        k3.setSize(60, 50);
        k1.setBounds(60, 100, 60, 50);
        k2.setBounds(160, 100, 60, 50);
        k3.setBounds(260, 100, 60, 50);

        oknoOpcji.add(dzwiek);
        dzwiek.setBounds(20, 160, 80, 50);

        oknoOpcji.add(ok);
        ok.setBounds(150, 220, 120, 50);
        ok.addActionListener(kl);

    }

    public void showAbout() {
        oknoAbout = new JFrame();
        oknoAbout.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        e.getWindow().dispose();
                        parent.setEnabled(true);
                    }
                }
        );
        oknoAbout.setSize(420, 260);
        oknoAbout.setVisible(true);
        oknoAbout.setTitle("O autorach");
        JTextArea opisAutorzy = new JTextArea("O autorach:\n"
                + "Adam Adamczuk - kierownik\n"
                + "Adrian Grzela\n"
                + "Krzyszftof Szubarczyk");
        opisAutorzy.setEditable(false);
        oknoAbout.setResizable(false);
        oknoAbout.add(opisAutorzy);

    }

    public void showPomoc() throws IOException {
        oknoPomocy = new JFrame();
        oknoPomocy.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        e.getWindow().dispose();
                        parent.setEnabled(true);
                    }
                }
        );
        oknoPomocy.setSize(750, 350);
        oknoPomocy.setVisible(true);
        oknoPomocy.setTitle("Pomoc");
        JTextArea opisPomoc = new JTextArea(" Każdy z graczy posiada po dwie plansze o wielkości, zazwyczaj, 10x10 pól.\n"
                + "Gra może się toczyć zarówno na kartce papieru, jak i specjalnej planszy.\n"
                + " Kolumny są oznaczone poprzez współrzędne literami od A do J i cyframi 1 do 10.\n"
                + " Na jednym z kwadratów gracz zaznacza swoje statki, których położenie będzie odgadywał przeciwnik.\n"
                + " Na drugim zaznacza trafione statki przeciwnika i oddane przez siebie strzały.\n"
                + " Statki ustawiane są w pionie lub poziomie , w taki sposób aby nie stykały się one ze sobą ani bokami,\n"
                + " ani rogami. Okręty są różnej wielkości i zazwyczaj więcej jest jednostek o mniejszej wielkości,\n"
                + " np. gracze mogą posiadać po jednym czteromasztowcu wielkości czterech kratek, dwóch trójmasztowcach\n"
                + " wielkości trzech kratek, trzech dwumasztowcach o wielkości dwóch kratek i po czterech jednomasztowcach.\n"
                + "Trafienie okrętu przeciwnika polega na strzale, który jest odgadnięciem położenia jakiegoś statku.\n"
                + " Strzały oddawane są naprzemiennie, poprzez podanie współrzędnych pola (np. B5). W przypadku strzału trafionego,\n"
                + " gracz kontynuuje strzelanie (czyli swój ruch) aż do momentu chybienia. Zatopienie statku ma miejsce wówczas,\n"
                + " gdy gracz odgadnie położenie całego statku. O chybieniu gracz informuje przeciwnika słowem „pudło”,\n"
                + " o trafieniu „trafiony ” lub „(trafiony) zatopiony. Wygrywa ten, kto pierwszy zatopi wszystkie statki przeciwnika.\n"
                + " Współcześnie istnieją także plansze do gry i wersje komputerowe, w których niepotrzebna jest\n"
                + " znajomość alfabetu ani cyfr.");
        opisPomoc.setEditable(false);
        oknoPomocy.setResizable(false);
        oknoPomocy.add(opisPomoc);
    }

    public void showWybor() {
        oknoWyboru = new JFrame();
        runGame = new JButton("Ustaw statki");
        runGame.addActionListener(rg);
        oknoWyboru.setSize(250, 390);
        oknoWyboru.setVisible(true);
        oknoWyboru.setTitle("Wybór gry");
        oknoWyboru.setResizable(false);
        oknoWyboru.setLayout(null);
        JLabel label = new JLabel("Wybór gry!!!");
        oknoWyboru.add(label);
        label.setSize(150, 50);
        label.setBounds(55, 0, 150, 50);
        label.setFont(new Font("Helvetica", Font.BOLD, 20));
        JLabel m4 = new JLabel("4-masztowce");
        JLabel m3 = new JLabel("3-masztowce");
        JLabel m2 = new JLabel("2-masztowce");
        JLabel m1 = new JLabel("1-masztowce");
        oknoWyboru.add(m4);
        oknoWyboru.add(m3);
        oknoWyboru.add(m2);
        oknoWyboru.add(m1);
        m4.setBounds(20, 70, 100, 25);
        m3.setBounds(20, 125, 100, 25);
        m2.setBounds(20, 180, 100, 25);
        m1.setBounds(20, 235, 100, 25);

        model4 = new SpinnerNumberModel(1, 1, 1, 1);
        model3 = new SpinnerNumberModel(1, 1, 2, 1);
        model2 = new SpinnerNumberModel(1, 1, 2, 1);
        model1 = new SpinnerNumberModel(1, 1, 3, 1);
        spinner4 = new JSpinner(model4);
        spinner4.setBounds(125, 70, 50, 25);
        oknoWyboru.add(spinner4);
        spinner3 = new JSpinner(model3);
        spinner3.setBounds(125, 125, 50, 25);
        oknoWyboru.add(spinner3);
        spinner2 = new JSpinner(model2);
        spinner2.setBounds(125, 180, 50, 25);
        oknoWyboru.add(spinner2);
        spinner1 = new JSpinner(model1);
        spinner1.setBounds(125, 235, 50, 25);
        oknoWyboru.add(spinner1);

        oknoWyboru.add(runGame);
        runGame.setBounds(50, 275, 150, 50);
        //runGame.addActionListener(kl);
    }

    public void UstawDomyslne() {
        k1.setBackground(Color.yellow);
        k2.setBackground(Color.gray);
        k3.setBackground(Color.red);

    }

    public void PobierzUstawienia() {

        kolor1 = k1.getBackground();
        kolor2 = k2.getBackground();
        kolor3 = k3.getBackground();
        sound = dzwiek.isSelected();
        Color[] tablicaKolorow = new Color[]{kolor1, kolor2, kolor3};
        PrzeslijUstawienia(tablicaKolorow, sound);

    }

    public void PrzeslijWybor() {

        int l4 = (int) spinner4.getValue();
        int l3 = (int) spinner3.getValue();
        int l2 = (int) spinner2.getValue();
        int l1 = (int) spinner1.getValue();

        int[] tmp = new int[]{l4, l3, l2, l1};

        this.parent.setStatki(tmp);
        this.parent.PrzeslijLiczby();

    }

    private void PrzeslijUstawienia(Color[] tablica, boolean sound) {
        parent.PrzeslijUstawienia(tablica, sound);
    }
}
