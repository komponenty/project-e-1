package pk.project.uzytkownik.internal;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Okno1 extends Okienka {

    private JMenuItem about = new JMenuItem("About");
    private JMenuItem zamykanie = new JMenuItem("Koniec");
    private JMenuItem pomoc = new JMenuItem("Pomoc");
    private JButton start = new JButton("Start");
    private JButton opcje = new JButton("Ustawienia");
    private JButton koniec = new JButton("Koniec");

    private int[] statki;
    private Uzytkownik user;

    public int[] getStatki() {
        return statki;
    }

    public void setStatki(int[] statki) {
        this.statki = statki;
    }

    public Okno1(Uzytkownik user) {
        this.user = user;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Statki");
        setSize(600, 400);
        setVisible(true);
        setResizable(false);
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu plik = new JMenu("Plik");

        plik.add(about);
        about.addActionListener(al);

        plik.add(zamykanie);
        zamykanie.addActionListener(al);

        menuBar.add(plik);
        menuBar.add(pomoc);
        pomoc.addActionListener(al);

        JLabel label = new JLabel("Witaj w grze STATKI!!!");
        add(label);
        label.setBounds(190, 0, 250, 50);
        label.setFont(new Font("Helvetica", Font.BOLD, 20));
        //tworzenie przycisków
        //start
        setLayout(null);
        add(start);
        start.setBounds(225, 60, 150, 50);
        start.addActionListener(al);
        //opcje
        add(opcje);
        opcje.setBounds(225, 160, 150, 50);
        opcje.addActionListener(al);
        //koniec
        add(koniec);
        koniec.setBounds(225, 260, 150, 50);
        koniec.addActionListener(al);
        UstawDomyslne();
        setParent(this);
        JLabel tlo = new JLabel();
        tlo.setIcon(new ImageIcon("okrety.jpeg"));        
        tlo.setBounds(0, 0, 600, 400);
        add(tlo);
    }

    private ActionListener al = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == zamykanie || e.getSource() == koniec) {
                dispose();
            } else if (e.getSource() == opcje) {
                setEnabled(false);
                showOpcje();

            } else if (e.getSource() == about) {
                setEnabled(false);
                showAbout();

            } else if (e.getSource() == pomoc) {
                try {
                    showPomoc();
                    setEnabled(false);
                } catch (IOException ex) {
                    Logger.getLogger(Okno1.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (e.getSource() == start) {
                dispose();
                showWybor();

            }

        }
    };

    public void PrzeslijLiczby() {
        this.user.WyslijLiczby(this.statki);
    }

    void PrzeslijUstawienia(Color[] tablica, boolean sound) {
        this.user.Ustawienia(tablica, sound);
    }

}
