package pk.project.contracts;

import java.awt.Color;

public interface IGra {
    void UstawLiczbeStatkow(int liczbaJednomasztowcow, int liczbaDwumasztowcow, int liczbaTrojmasztowcow, int liczbaCzteromasztowcow);
    void StartGry();
    void StopGry();
    boolean SprawdzCzyKoniec();
    void UstawKolory(Color[] tablicaKolorow);
    void UstawDzwiek(boolean sound);
}