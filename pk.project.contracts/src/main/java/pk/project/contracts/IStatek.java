package pk.project.contracts;

import java.awt.Color;
import javax.swing.JButton;

public interface IStatek {
    void DodajTrafienie(int x, int y, Object plansza, boolean czyUser);
    void DodajDoPlanszy(Object plansza, JButton button, int dlugoscStatku, Color[] tablica, boolean czyWidoczne);
    boolean getCzyOK();
    boolean getCzyZatopiony();
    void setLicznik(int licznik);
    int getLicznik();
}
