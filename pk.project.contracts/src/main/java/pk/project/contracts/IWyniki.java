package pk.project.contracts;

public interface IWyniki {    
    void ObliczCelnosc(double liczbaStrzalow, double liczbaTrafionych);
    double LiczPunkty(int punkty, double czas, int liczbaStatkow);
    void Podsumuj();
}
