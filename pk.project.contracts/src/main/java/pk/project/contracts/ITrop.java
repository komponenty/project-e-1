package pk.project.contracts;

import java.awt.Point;

public interface ITrop {
    boolean CzyJest();
    void AktualizujTrop(boolean zmiana);
    void ZacznijTrop(int x, int y);   
    void PrzerwijTrop();
    Point Cel();
}
