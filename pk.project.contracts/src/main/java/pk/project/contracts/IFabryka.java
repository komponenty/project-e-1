package pk.project.contracts;

import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import java.awt.Color;

public interface IFabryka {
    Object ZwrocPlansze(int rozmiarPlanszy, int rozmiarPola, int pozycjaX, int pozycjaY, Color kolorPola, BufferedImage obrazek, JFrame frame);
}
